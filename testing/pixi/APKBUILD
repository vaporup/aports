# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=pixi
pkgver=0.16.1
pkgrel=0
pkgdesc="A package management and workflow tool"
url="https://github.com/prefix-dev/pixi"
# !s390x: nix crate fails to build
# !armhf: openssl fails to build
arch="all !s390x !armhf"
license="BSD-3-Clause"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	perl
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
# https://github.com/prefix-dev/pixi/issues/821
options="net !check"
source="$pkgname-$pkgver.tar.gz::https://github.com/prefix-dev/pixi/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
	mkdir -p completions/
}

build() {
	cargo auditable build --frozen --release
	local _completion="target/release/$pkgname completion"
	$_completion --shell bash > "completions/$pkgname"
	$_completion --shell fish > "completions/$pkgname.fish"
	$_completion --shell zsh  > "completions/_$pkgname"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm 664 "completions/$pkgname" -t "$pkgdir/usr/share/bash-completion/completions/"
	install -Dm 664 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d/"
	install -Dm 664 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions/"
}

sha512sums="
f957a55a008b0722572ee5450880680523fb8d49269a4c3e9d73485fcd2cce6078f2cb2268f149fb1dd0a57d688c3c058d0abc98b5503494a760373a454f278d  pixi-0.16.1.tar.gz
"
