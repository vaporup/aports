# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Krassy Boykinov <kboykinov@teamcentrixx.com>
pkgname=glib-networking
pkgver=2.78.1
pkgrel=0
pkgdesc="Networking support for GLib"
url="https://gitlab.gnome.org/GNOME/glib-networking"
arch="all"
license="LGPL-2.0-or-later"
depends="ca-certificates gsettings-desktop-schemas"
makedepends="
	bash
	glib-dev
	gnutls-dev
	gsettings-desktop-schemas-dev
	libgcrypt-dev
	libproxy-dev
	meson
	p11-kit-dev
	"
subpackages="$pkgname-dbg $pkgname-lang"
source="https://download.gnome.org/sources/glib-networking/${pkgver%.*}/glib-networking-$pkgver.tar.xz"
options="!check" # new p11-kit makes a few of these fail, run them next release

# secfixes:
#   2.64.3-r0:
#     - CVE-2020-13645

build() {
	abuild-meson \
		-Db_lto=true \
		-Dopenssl=disabled \
		-Dgnutls=enabled \
		. build
	meson compile -C build
}

check() {
	meson test --no-rebuild --print-errorlogs -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
dc8076d3ff1c97c44c7ba04f74bed50117b07f703efc1bc4cc44989ffcfada0ab49813556ad5bb1831dfb114c4f74ec7bfe08c9b6b514ae7049700211606b288  glib-networking-2.78.1.tar.xz
"
